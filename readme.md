 
# Docker images
```
docker build --build-arg COLOR='blue' -t marcinwierzbicki/bluegreen:blue .
docker build --build-arg COLOR='green' -t marcinwierzbicki/bluegreen:green .


docker push marcinwierzbicki/bluegreen:blue
docker push marcinwierzbicki/bluegreen:green


docker run --name bluegreen-blue -d -p 8080:80 marcinwierzbicki/bluegreen:blue 
docker run --name bluegreen-green -d -p 8081:80 marcinwierzbicki/bluegreen:green 
```


# Helm
```
helm create bluegreen-chart
cd bluegreen-chart
rm -rf templates/*
helm install -n bluegreen --create-namespace bluegreen ./bluegreen-chart 
helm upgrade -n bluegreen bluegreen ./bluegreen-chart 
```

# Features
## Values
### file
```
values.yaml
```

### set
```
helm upgrade -n bluegreen bluegreen --set blueGreen.currentVersion=green ./bluegreen-chart 
```

### built in
```
{{ .Release.Name }}
```

# Functions
```
default STRING
```
```
indent NUMBER
```
```
quote
```
```
kebabcase STRING
```
https://helm.sh/docs/chart_template_guide/function_list/#kebabcase

# Flow control
```
{{ .Values.favorite.food | upper | quote }}
```